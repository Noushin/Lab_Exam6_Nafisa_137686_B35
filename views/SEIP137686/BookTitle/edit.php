<?php
require_once("../../../vendor/autoload.php");
use App\Message\Message;

if(!isset( $_SESSION)) session_start();
echo Message::message();


use App\BookTitle\BookTitle;

$objBookTitle = new BookTitle();
$objBookTitle->setData($_GET);

    $oneData= $objBookTitle-> view("obj");





?>
<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Bootstrap Login Form Template</title>

    <!-- CSS -->
    <link rel="stylesheet" href="http://fonts.googleapis.com/css?family=Roboto:400,100,300,500">
    <link rel="stylesheet" href="../../../resourse/assets/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="../../../resourse/assets/font-awesome/css/font-awesome.min.css">
    <link rel="stylesheet" href="../../../resourse/assets/css/form-elements.css">
    <link rel="stylesheet" href="../../../resourse/assets/css/style.css">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->


</head>

<body>

<!-- Top content -->
<div class="top-content">


    <div class="container">

        <div class="row">
            <div class="col-sm-6 col-sm-offset-3">
                <div class="form-top">
                    <div class="form-top-left">
                        <h3>Login to our site</h3>
                        <p>Edit Book Title and Author Name :</p>
                    </div>
                    <div class="form-top-right">
                        <i class="fa fa-book"></i>
                    </div>
                </div>
                <div class="form-bottom">
                    <form role="form" action="update.php" method="post" class="login-form">
                        <input type="hidden" name="id" value="<?php echo $oneData->id ?>">
                        <div class="form-group">
                            <label class="sr-only" for="form-book_title">Book Title</label>
                            <input type="text" name="book_title" value="<?php echo $oneData->book_title?>" class="form-book_title form-control" id="form-book_title">
                        </div>
                        <div class="form-group">
                            <label class="sr-only" for="form-author_name">Author Name</label>
                            <input type="text" name="author_name" value="<?php echo $oneData->author_name?>" class="form-author_name form-control" id="form-author_name">
                        </div>
                        <button type="submit" class="btn">Update!</button>
                    </form>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-6 col-sm-offset-3 social-login">

            </div>
        </div>
    </div>
</div>




<!-- Javascript -->
<script src="../../../resourse/assets/js/jquery-1.11.1.min.js"></script>
<script src="../../../resourse/assets/bootstrap/js/bootstrap.min.js"></script>
<script src="../../../resourse/assets/js/jquery.backstretch.min.js"></script>
<script src="../../../resourse/assets/js/scripts.js"></script>

<!--[if lt IE 10]>
<script src="../../../resourse/assets/js/placeholder.js"></script>
<![endif]-->

</body>

</html>
