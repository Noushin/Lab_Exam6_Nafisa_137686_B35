<head>
    <link rel="stylesheet" href="../../../resourse/assets/bootstrap/css/bootstrap.min.css">
    <script src="../../../resourse/assets/bootstrap/js/bootstrap.min.js"></script>
</head>

<?php
require_once("../../../vendor/autoload.php");

use App\Gender\Gender;
use App\Message\Message;



$objGender = new Gender();


$allData = $objGender->index("obj");

$serial = 1;

echo "<table border='2px'>";
echo "<th> Serial </th><th> ID </th><th> Name </th><th> Gender </th><th> Action </th>";
foreach($allData as $oneData){

    echo "<tr>";
    echo "<td> $serial </td>";
    echo "<td> $oneData->id </td>";
    echo "<td> $oneData->name </td>";
    echo "<td> $oneData->gender </td>";

    echo "
       <td>
             <a href='view.php?id=$oneData->id'><button class='btn-info'>View</button></a>
             <a href='edit.php?id=$oneData->id'><button class='btn-primary'>Edit</button></a>
             <a href='delete.php?id=$oneData->id'><button class='btn-danger'>Delete</button></a>
             <a href='trash.php?id=$oneData->id'><button class='btn-primary'>trash</button></a>
       </td>

     ";


    echo "</tr>";
    $serial++;
}// end of foreach Loop

echo "</table>";




?>