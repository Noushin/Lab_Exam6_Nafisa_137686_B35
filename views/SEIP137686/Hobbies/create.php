<?php
require_once("../../../vendor/autoload.php");
use App\Message\Message;

if(!isset( $_SESSION)) session_start();
echo Message::message();



?>
<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Bootstrap Login Form Template</title>

    <!-- CSS -->
    <link rel="stylesheet" href="http://fonts.googleapis.com/css?family=Roboto:400,100,300,500">
    <link rel="stylesheet" href="../../../resourse/assets/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="../../../resourse/assets/font-awesome/css/font-awesome.min.css">
    <link rel="stylesheet" href="../../../resourse/assets/css/form-elements.css">
    <link rel="stylesheet" href="../../../resourse/assets/css/style.css">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->


</head>

<body>

<!-- Top content -->
<div class="top-content">


    <div class="container">

        <div class="row">
            <div class="col-sm-6 col-sm-offset-3">

                <fieldset class="control-group ">
                    <legend>Add your Hobbies</legend>
                    <form action="store.php" method="post" class="form-inline">
                        <div class="input_form">
                            <label for="hobbies"> &nbsp;  &nbsp; Hobbies &nbsp; </label>
                            <label class="checkbox">
                                <input type="checkbox" name="hobbies[]" id="inlineCheckbox2" value="programming"> Programming
                            </label>
                            <label class="checkbox">
                                <input type="checkbox" name="hobbies[]" id="inlineCheckbox2" value="fishing"> Fishing
                            </label>
                            <label class="checkbox">
                                <input type="checkbox" name="hobbies[]" id="inlineCheckbox3" value="hacking"> Hacking
                            </label>
                            <label class="checkbox">
                                <input type="checkbox" name="hobbies[]" id="inlineCheckbox1" value="gardening"> Gardening
                            </label>
                            <label class="checkbox">
                                <input type="checkbox" name="hobbies[]" id="inlineCheckbox2" value="racing"> Racing
                            </label>
                            <label class="checkbox">
                                <input type="checkbox" name="hobbies[]" id="inlineCheckbox3" value="playing"> Playing
                            </label>
                        </div>
                        <div class="input_form">
                            <label for="Name">Name</label>
                            <input type="text" id="Name" class="input-xxlarge" name="name">
                        </div>
                        <div>
                            &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;&nbsp;
                            <input type="submit" class="btn btn-success" value="submit">
                        </div>
                    </form>
                </fieldset>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-6 col-sm-offset-3 social-login">

            </div>
        </div>
    </div>
</div>




<!-- Javascript -->
<script src="../../../resourse/assets/js/jquery-1.11.1.min.js"></script>
<script src="../../../resourse/assets/bootstrap/js/bootstrap.min.js"></script>
<script src="../../../resourse/assets/js/jquery.backstretch.min.js"></script>
<script src="../../../resourse/assets/js/scripts.js"></script>

<!--[if lt IE 10]>
<script src="../../../resourse/assets/js/placeholder.js"></script>
<![endif]-->

</body>

</html>
