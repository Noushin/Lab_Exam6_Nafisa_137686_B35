<?php
require_once("../../../vendor/autoload.php");
use App\Message\Message;



use App\ProfilePicture\ProfilePicture;

$objBookTitle = new ProfilePicture();
$objBookTitle->setData($_GET);

$oneData= $objBookTitle-> view("obj");





?>
<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Bootstrap Login Form Template</title>

    <!-- CSS -->
    <link rel="stylesheet" href="http://fonts.googleapis.com/css?family=Roboto:400,100,300,500">
    <link rel="stylesheet" href="../../../resourse/assets/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="../../../resourse/assets/font-awesome/css/font-awesome.min.css">
    <link rel="stylesheet" href="../../../resourse/assets/css/form-elements.css">
    <link rel="stylesheet" href="../../../resourse/assets/css/style.css">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->


</head>

<body>

<!-- Top content -->
<div class="top-content">


    <div class="container">


        <div class="col-sm-6 col-sm-offset-3 form-box">
            <div class="form-top">
                <div class="form-top-left">
                    <h3>Profile Picture</h3>
                    <p>Update Name And Profile Picture</p>
                </div>
                <div class="form-top-right">
                    <i class="fa fa-book"></i>
                </div>
            </div>
            <div class="form-bottom">
                <form role="form" action="update.php" method="post" class="login-form" enctype="multipart/form-data">
                    <input type="hidden" name="id" value="<?php echo $oneData->id?>">
                    <div class="form-group">
                        <label class="sr-only" for="form-book_title">Name</label>
                        <input type="text" name="name" value="<?php echo $oneData->name?>" class="form-username form-control" id="form-username">
                    </div>
                    <div class="form-group">
                        <label class="sr-only" for="form-author_name">Image</label>
                        Image:
                        <input type="file" name="image"  value="<?php echo $oneData->image?>" >
                        <img src="../../../resourse/Upload/<?php echo $oneData->image?>" height="200px" width="200px">


                    </div>
                    <button type="submit" class="btn">Update</button>
                </form>
            </div>
        </div>


    </div>
</div>








<!-- Javascript -->
<script src="../../../resourse/assets/js/jquery-1.11.1.min.js"></script>
<script src="../../../resourse/assets/bootstrap/js/bootstrap.min.js"></script>
<script src="../../../resourse/assets/js/jquery.backstretch.min.js"></script>
<script src="../../../resourse/assets/js/scripts.js"></script>

<!--[if lt IE 10]>
<script src="../../../resourse/assets/js/placeholder.js"></script>
<![endif]-->

</body>

</html>
