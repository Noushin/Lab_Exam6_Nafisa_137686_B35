<?php
namespace App\SummaryOfOrganization;
use App\Message\Message;
use App\Utility\Utility;
use App\Model\Database as DB;
use PDO;


class SummaryOfOrganization extends DB{
    public $id="";
    public $name="";
    public $summary="";

    public function __construct(){
        parent:: __construct();
        if(!isset( $_SESSION)) session_start();
    }

    public function setData($data=NULL){

        if(array_key_exists('id',$data)){
            $this->id = $data['id'];
        }

        if(array_key_exists('name',$data)){
            $this->name = $data['name'];
        }

        if(array_key_exists('summary',$data)){
            $this->summary = $data['summary'];
        }
    }


    public function store(){

        $arrData  = array($this->name,$this->summary);

        $sql = "INSERT INTO summary_of_organization ( name, summary) VALUES ( ?, ?)";

        $STH = $this->DBH->prepare($sql);


        $STH->execute($arrData);

        Message::message("<div id='msg'></div><h3 align='center'>[ Name: $this->name ] , [ Summary: $this->summary ] <br> Data Has Been Inserted Successfully!</h3></div>");


        Utility::redirect('create.php');

    }


    public function index($fetchMode='ASSOC'){
        $sql = "SELECT * from summary_of_organization where is_deleted = 'No' ";
        $STH = $this->DBH->query($sql);

        $fetchMode = strtoupper($fetchMode);
        if(substr_count($fetchMode,'OBJ') > 0)
            $STH->setFetchMode(PDO::FETCH_OBJ);
        else
            $STH->setFetchMode(PDO::FETCH_ASSOC);

        $arrAllData  = $STH->fetchAll();
        return $arrAllData;


    }// end of index();

    public function view($fetchMode = 'ASSOC')
    {

        $STH = $this->DBH->query('SELECT * from summary_of_organization where id =' . $this->id);

        $fetchMode = strtoupper($fetchMode);
        if (substr_count($fetchMode, 'OBJ') > 0)
            $STH->setFetchMode(PDO::FETCH_OBJ);
        else
            $STH->setFetchMode(PDO::FETCH_ASSOC);

        $arrOneData = $STH->fetch();
        return $arrOneData;


    }// end of view();

    public function update(){

        $arrData = array ($this->name, $this->summary);
        $sql = "UPDATE summary_of_organization SET name = ?, summary = ? WHERE id =".$this->id;

        $STH = $this->DBH->prepare($sql);
        $STH->execute($arrData);

        Utility::redirect('index.php');

    }// end of update()


    public function delete(){

        $sql = "Delete from summary_of_organization where id=".$this->id;

        $STH = $this->DBH->prepare($sql);

        $STH->execute();

        Utility::redirect('index.php');

    }// end of delete()


    public function trash(){

        $sql = "Update summary_of_organization SET is_deleted=NOW() where id=".$this->id;

        $STH = $this->DBH->prepare($sql);

        $STH->execute();

        Utility::redirect('index.php');


    }// end of trash()



    public function trashed($fetchMode='ASSOC'){
        $sql = "SELECT * from summary_of_organization where is_deleted <> 'No' ";
        $STH = $this->DBH->query($sql);

        $fetchMode = strtoupper($fetchMode);
        if(substr_count($fetchMode,'OBJ') > 0)
            $STH->setFetchMode(PDO::FETCH_OBJ);
        else
            $STH->setFetchMode(PDO::FETCH_ASSOC);

        $arrAllData  = $STH->fetchAll();
        return $arrAllData;


    }// end of trashed();





    public function recover(){

        $sql = "Update summary_of_organization SET is_deleted='No' where id=".$this->id;

        $STH = $this->DBH->prepare($sql);

        $STH->execute();

        Utility::redirect('index.php');

    }// end of recover();


}

?>




