<?php
namespace App\Hobbies;
use App\Message\Message;
use App\Utility\Utility;
use App\Model\Database as DB;
use PDO;

class Hobbies extends DB{
    public $id="";
    public $name="";
    public $hobbies=array();

    public function __construct(){
        parent:: __construct();
        if(!isset( $_SESSION)) session_start();
    }

    public function setData($data=NULL){

        if(array_key_exists('id',$data)){
            $this->id = $data['id'];
        }

        if(array_key_exists('name',$data)){
            $this->name = $data['name'];
        }

        if(array_key_exists('hobbies',$data)){

            $hobbies = implode(',', $_POST['hobbies']);
            $this->hobbies = $hobbies;

        }
    }


    public function store(){

        $arrData  = array($this->name,$this->hobbies);

        $sql = "INSERT INTO hobbies ( name, hobbies) VALUES ( ?, ?)";

        $STH = $this->DBH->prepare($sql);


        $STH->execute($arrData);

        Message::message("<div id='msg'></div><h3 align='center'>[ Name: $this->name ] , [ Hobbies: $this->hobbies ] <br> Data Has Been Inserted Successfully!</h3></div>");


        Utility::redirect('create.php');

    }


    public function index($fetchMode='ASSOC'){
        $sql = "SELECT * from hobbies where is_deleted = 'No' ";
        $STH = $this->DBH->query($sql);

        $fetchMode = strtoupper($fetchMode);
        if(substr_count($fetchMode,'OBJ') > 0)
            $STH->setFetchMode(PDO::FETCH_OBJ);
        else
            $STH->setFetchMode(PDO::FETCH_ASSOC);

        $arrAllData  = $STH->fetchAll();
        return $arrAllData;


    }// end of index();


    public function view($fetchMode = 'ASSOC')
    {

        $STH = $this->DBH->query('SELECT * from hobbies where id =' . $this->id);

        $fetchMode = strtoupper($fetchMode);
        if (substr_count($fetchMode, 'OBJ') > 0)
            $STH->setFetchMode(PDO::FETCH_OBJ);
        else
            $STH->setFetchMode(PDO::FETCH_ASSOC);

        $arrOneData = $STH->fetch();
        return $arrOneData;


    }// end of view();

    public function update(){

        $arrData = array ($this->name, $this->hobbies);
        $sql = "UPDATE hobbies SET name = ?, hobbies = ? WHERE id =".$this->id;

        $STH = $this->DBH->prepare($sql);
        $STH->execute($arrData);

        Utility::redirect('index.php');

    }// end of update()


    public function delete(){

        $sql = "Delete from hobbies where id=".$this->id;

        $STH = $this->DBH->prepare($sql);

        $STH->execute();

        Utility::redirect('index.php');

    }// end of delete()


    public function trash(){

        $sql = "Update hobbies SET is_deleted=NOW() where id=".$this->id;

        $STH = $this->DBH->prepare($sql);

        $STH->execute();

        Utility::redirect('index.php');


    }// end of trash()



    public function trashed($fetchMode='ASSOC'){
        $sql = "SELECT * from hobbies where is_deleted <> 'No' ";
        $STH = $this->DBH->query($sql);

        $fetchMode = strtoupper($fetchMode);
        if(substr_count($fetchMode,'OBJ') > 0)
            $STH->setFetchMode(PDO::FETCH_OBJ);
        else
            $STH->setFetchMode(PDO::FETCH_ASSOC);

        $arrAllData  = $STH->fetchAll();
        return $arrAllData;


    }// end of trashed();





    public function recover(){

        $sql = "Update hobbies SET is_deleted='No' where id=".$this->id;

        $STH = $this->DBH->prepare($sql);

        $STH->execute();

        Utility::redirect('index.php');

    }// end of recover();



}
//$objHobbies = new Hobbies();
?>



