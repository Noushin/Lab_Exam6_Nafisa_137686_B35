<?php
namespace App\Gender;
use App\Message\Message;
use App\Utility\Utility;
use App\Model\Database as DB;
use PDO;

class Gender extends DB{
    public $id="";
    public $name="";
    public $gender="";

    public function __construct(){
        parent:: __construct();
        if(!isset( $_SESSION)) session_start();
    }

    public function setData($data=NULL){

        if(array_key_exists('id',$data)){
            $this->id = $data['id'];
        }

        if(array_key_exists('name',$data)){
            $this->name = $data['name'];
        }

        if(array_key_exists('gender',$data)){
            $this->gender = $data['gender'];
        }
    }


    public function store(){

        $arrData  = array($this->name,$this->gender);

        $sql = "INSERT INTO gender ( name, gender) VALUES ( ?, ?)";

        $STH = $this->DBH->prepare($sql);


        $STH->execute($arrData);

        Message::message("<div id='msg'></div><h3 align='center'>[ name: $this->name ] , [ Gender: $this->gender ] <br> Data Has Been Inserted Successfully!</h3></div>");


        Utility::redirect('create.php');

    }


    public function index($fetchMode='ASSOC'){
        $sql = "SELECT * from gender where is_deleted = 'No' ";
        $STH = $this->DBH->query($sql);

        $fetchMode = strtoupper($fetchMode);
        if(substr_count($fetchMode,'OBJ') > 0)
            $STH->setFetchMode(PDO::FETCH_OBJ);
        else
            $STH->setFetchMode(PDO::FETCH_ASSOC);

        $arrAllData  = $STH->fetchAll();
        return $arrAllData;


    }// end of index();



    public function view($fetchMode='ASSOC'){

        $sql = 'SELECT * from gender where id='.$this->id;

        $STH = $this->DBH->query($sql);

        $fetchMode = strtoupper($fetchMode);
        if(substr_count($fetchMode,'OBJ') > 0)
            $STH->setFetchMode(PDO::FETCH_OBJ);
        else
            $STH->setFetchMode(PDO::FETCH_ASSOC);

        $arrOneData  = $STH->fetch();
        return $arrOneData;


    }// end of view();


    public function update(){

        $arrData = array ($this->name, $this->gender);
        $sql = "UPDATE gender SET name = ?, gender = ? WHERE id =".$this->id;

        $STH = $this->DBH->prepare($sql);
        $STH->execute($arrData);

        Utility::redirect('index.php');

    }// end of update()


    public function delete(){

        $sql = "Delete from gender where id=".$this->id;

        $STH = $this->DBH->prepare($sql);

        $STH->execute();

        Utility::redirect('index.php');

    }// end of delete()


    public function trash(){

        $sql = "Update gender SET is_deleted=NOW() where id=".$this->id;

        $STH = $this->DBH->prepare($sql);

        $STH->execute();

        Utility::redirect('index.php');


    }// end of trash()



    public function trashed($fetchMode='ASSOC'){
        $sql = "SELECT * from gender where is_deleted <> 'No' ";
        $STH = $this->DBH->query($sql);

        $fetchMode = strtoupper($fetchMode);
        if(substr_count($fetchMode,'OBJ') > 0)
            $STH->setFetchMode(PDO::FETCH_OBJ);
        else
            $STH->setFetchMode(PDO::FETCH_ASSOC);

        $arrAllData  = $STH->fetchAll();
        return $arrAllData;


    }// end of trashed();





    public function recover(){

        $sql = "Update gender SET is_deleted='No' where id=".$this->id;

        $STH = $this->DBH->prepare($sql);

        $STH->execute();

        Utility::redirect('index.php');

    }// end of recover();



}// end of Gender Class

?>